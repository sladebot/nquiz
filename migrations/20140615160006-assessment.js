'use strict';

var dbm = require('db-migrate')
  , type = dbm.dataType;

exports.up = function(db, callback) {
  db.createTable('asssessments', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    email: 'string',
    firstName: 'string',
    lastName: 'string',
    sessionToken: 'string',
    answers: 'text',
    token: 'string'
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('asssessments', callback);
};
