'use strict';

var dbm = require('db-migrate')
  , type = dbm.dataType
  , async = require('async');

exports.up = function(db, callback) {
  async.series([
    db.renameTable.bind(db, 'asssessments', 'assessments'),
    db.removeColumn.bind(db, 'assessments', 'id'),
    db.changeColumn.bind(db, 'assessments', 'token', {primaryKey: true})
  ], callback);
};

exports.down = function(db, callback) {
  async.series([
    db.renameTable.bind(db, 'assessments', 'asssessments'),
    db.addColumn.bind(db, 'asssessments', 'id', {type: 'int'}),
    db.changeColumn.bind(db, 'asssessments', 'token', {primaryKey: false})
  ], callback);
};
