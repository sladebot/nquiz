"use strict";

var mask = require('json-mask');

module.exports = function(sequelize, DataTypes) {
  var Answer = sequelize.define('Answer', {
    questionId: {
      type: DataTypes.INTEGER,
      validate: {
        notNull: {
          args: false,
          msg: "Can't be blank"
        }
      }
    },
    text: {
      type: DataTypes.STRING,
      validate: {
        notNull: {
          args: false,
          msg: "Can't be blank"
        }
      }
    },
    isAnswer: DataTypes.BOOLEAN,
  },{
    timestamps: false,
    tableName: 'answers',
    instanceMethods: {
      toJson: function() {
        return mask(this, "id,text,isAnswer");
      }
    },
    classMethods: {
      associate: function(models) {
        Answer.belongsTo(models.Question);
      }
    }
  });

  return Answer;
};
