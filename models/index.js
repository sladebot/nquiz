"use strict";

var consoleLogger = function(msg) {
  console.log(msg);
};

var fs = require('fs')
    , path      = require('path')
    , Sequelize = require('sequelize')
    , lodash    = require('lodash')
    , env = process.env.NODE_ENV || 'dev'
    , defaultDatabaseUrl = process.env.DATABASE_URL
    , databaseUrl = defaultDatabaseUrl || JSON.parse(fs.readFileSync(path.join("__dirname", "../", "database.json")))[env]
    , extendModel = require('./extension.js')
    , models = {}
    , sequelize = new Sequelize(databaseUrl,{
    logging: process.env.LOGGING ? consoleLogger : false,
    pool: { maxConnections: 5, maxIdleTime: 30}
  });
fs.readdirSync(__dirname).filter(function(file) {
  return (file.indexOf('.') !== 0) && (file !== 'index.js') && (file !== 'extension.js');
}).forEach(function(file) {
  var model = sequelize.import(path.join(__dirname, file));
  extendModel(model);
  models[model.name] = model;
});

Object.keys(models).forEach(function(modelName) {
  if ('associate' in models[modelName]) {
    models[modelName].associate(models);
  }
});

module.exports = lodash.extend({
  sequelize: sequelize,
  Sequelize: Sequelize
}, models);
