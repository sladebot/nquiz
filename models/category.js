'use strict';

var mask = require('json-mask')
  , _ = require('lodash')
  , Q = require('q');

module.exports = function(sequelize, DataTypes) {
  var Category = sequelize.define('Category', {
    name: {
      type: DataTypes.STRING,
      validate: {
        notNull: {
          args: false,
          msg: "Can't be blank"
        }
      }
    },
  }, {
    timestamps: false,
    tableName: 'categories',
    classMethods: {
      associate: function(models) {
        Category.hasMany(models.Question, {through: 'categoryQuestions'});
      }
    },
    instanceMethods: {
      toJson: function() {
        return mask(this, 'id,name');
      },
      questions: function(params) {
        var defered = Q.defer();
        this.getQuestions(params)
        .success(function(questions) {
          defered.resolve(_.map(questions, function(question) { return question.toJson(); }));
        })
        .fail(function(err) {
          defered.reject(err);
        });
        return defered.promise;
      }
    }
  });

  return Category;
};
