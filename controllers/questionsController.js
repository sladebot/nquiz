"use strict";

var models = require("../models")
  , Question = models.Question
  , Answer = models.Answer
  , _ = require('lodash');

exports.index = function(req, res) {
  Question.all({ transaction: req.transaction }).success(function(questions) {
    res.json(_.map(questions, function(question) {
      return question.toJson();
    }));
  });
};

exports.manage = function(req, res) {
  res.render('questions', { title: 'Manage Questions' });
};

exports.show = function(req, res) {
  Question.findBy({where: {id: req.param('id')}, include: [Answer]}, {transaction: req.transaction})
  .then(function(question) {
    res.send(200, {
      question: question.toJson(),
      answers: _.map(question.answers, function(answer) { return answer.toJson(); }),
      type: question.type()
    });
  })
  .fail(function(err) {
    res.send(404);
  });
};

var createAnswerFromParams = function(questionId, params) {
  return _.map(params, function(param) {
    return {
      text: param.text,
      questionId: questionId,
      isAnswer: param.isAnswer === 'true'
    };
  });
};

exports.create = function(req, res) {
  var answerParams = req.param('answers');

  Question.create({text: req.param('text'), reason: req.param('reason')}, {transaction: req.transaction}).success(function(question) {
    if (answerParams) {
      var options = { fields: ['text', 'questionId', 'isAnswer'], validate: true, transaction: req.transaction };
      Answer.bulkCreate(createAnswerFromParams(question.id, answerParams), options)
      .success(function() {
        res.send(200, question);
      });
    } else {
      res.send(200, question);
    }
  }).error(function(err) {
    res.send(400, err);
  });
};

exports.update = function(req, res) {
  Question.findBy(req.param('id'), {transaction: req.transaction})
  .then(function(question) {
    Question.update({ text: req.param('text'), reason: req.param('reason')}, {id: question.id}, {transaction: req.transaction})
    .success(function() {
      res.send(200);
    }).error(function(err) {
      res.send(400, err);
    });
  })
  .fail(function() {
    res.send(404);
  });
};

exports.delete = function(req, res) {
  var condition = {id: req.param('id')};

  Question.findBy(condition, {transaction: req.transaction})
  .then(function(question) {
    Question.destroy(condition, {transaction: req.transaction}).success(function() {
      res.send(200);
    });
  }).fail(function() {
    res.send(404);
  });
};
