'use strict';

var models = require('../models')
  , User = models.User;

exports.create = function(req, res) {
  User.create({
    email: req.param('email'),
    password: req.param('password'),
    firstName: req.param('firstName'),
    lastName: req.param('lastName')
  }, { transaction: req.transaction })
  .success(function(user) {
    res.send(200, user.toJson());
  })
  .fail(function(err) {
    res.send(400, err);
  });
};
