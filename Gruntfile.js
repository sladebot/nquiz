'use strict';

var request = require('request')
  , fs = require('fs')
  , path = require('path')
  , _ = require('lodash');

module.exports = function (grunt) {
  // show elapsed time at the end
  require('time-grunt')(grunt);
  // load all grunt tasks
  require('load-grunt-tasks')(grunt);

  var clientSideJsPath = 'public/js/src';
  var clientSideJsFiles = _.where(fs.readdirSync(clientSideJsPath), function(file) {
    return fs.lstatSync(path.join(clientSideJsPath,file)).isFile() && file !== 'common.js';
  });

  var reloadPort = 35729, files;

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    develop: {
      server: {
        file: 'app.js'
      }
    },
    simplemocha: {
      unit: {
        options: {
          reporter: 'list'
        },
        src: 'test/unit/**/*.js'
      },
      functional: {
        options: {
          reporter: 'list'
        },
        src: 'test/functional/**/*.js'
      }
    },
    less: {
      development: {
        options: {
        },
        files: {
          'public/css/base.css': 'public/less/base.less'
        }
      }
    },
    clean: {
      build: ['dist/*'],
      temp: ['build'],
      generate: ['./public/js/*.js'],
    },
    copy: {
      app: {
        files: [
          {expand: true, src: ['*.js', '*.json', 'postinstall.sh', 'Procfile'], dest: 'dist/', filter: 'isFile'},
          {expand: true, src: ['./config/*', './middleware/*', 'migrations/*', 'models/*', 'controllers/*', 'helpers/*', 'views/*'], dest: 'dist/'},
          {expand: true, src: ['./public/css/*', './public/img/*', 'public/fonts/*'], dest:'dist/'}
        ]
      },
      js: {
        files: [
          {expand: true, flatten: true, src: ['./build/*.js', './build/js/require.js'], dest:'public/js/'}
        ]
      }
    },
    requirejs: {
      compile: {
        options: {
          baseUrl: path.join(clientSideJsPath, 'lib'),
          mainConfigFile: path.join(clientSideJsPath, 'common.js'),
          dir: 'build/js/',
          skipDirOptimize: true,
          logLevel: 0,
          modules: _.map(clientSideJsFiles, function(file) {
            return {name: '../' + path.basename(file, '.js')};
          })
        }
      }
    },
    uglify: {
      js: {
        files: _.reduce(clientSideJsFiles, function(result, file) {
          result[path.join('dist/public/js',file)] = [path.join(clientSideJsPath, '../', file)];
          return result;
        }, {'dist/public/js/require.js': [ path.join(clientSideJsPath, '../require.js') ]})
      }
    },
    express: {
      test: {
        options: {
          port: 5000,
          node_env: 'test',
          script: './app.js'
        }
      },
    },
    selenium: {
      options: {
        jar: './tools/selenium-server-standalone-2.41.0.jar',
        port: 4444
      },
      start: {
      },
    },
    compress: {
      main: {
        options: {
          archive: 'dist.zip'
        },
        files: [
          {src: ['dist/**']} // includes files in path
        ]
      }
    },
    watch: {
      options: {
        nospawn: true,
        livereload: reloadPort
      },
      server: {
        files: [
          'app.js',
          'models/*.js',
          'controllers/*.js'
        ],
        tasks: ['develop', 'delayed-livereload']
      },
      less: {
        files: ['public/less/*.less'],
        options: {
          livereload: reloadPort
        },
        tasks: ['less']
      },
      js: {
        files: ['public/js/src/**', ],
        options: {
          livereload: reloadPort
        },
        tasks: ['buildjs']
      },
      jade: {
        files: ['views/*.jade'],
        options: {
          livereload: reloadPort
        }
      }
    }
  });

  grunt.config.requires('watch.server.files');
  files = grunt.config('watch.server.files');
  files = grunt.file.expand(files);

  grunt.loadNpmTasks('grunt-simple-mocha');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-selenium-simple');
  grunt.loadNpmTasks('grunt-express-server');
  grunt.loadNpmTasks('grunt-contrib-compress');

  grunt.registerTask('delayed-livereload', 'Live reload after the node server has restarted.', function () {
    var done = this.async();
    setTimeout(function () {
      request.get('http://localhost:' + reloadPort + '/changed?files=' + files.join(','),  function (err, res) {
          var reloaded = !err && res.statusCode === 200;
          if (reloaded) {
            grunt.log.ok('Delayed live reload successful.');
          } else {
            grunt.log.error('Unable to make a delayed live reload.');
          }
          done(reloaded);
        });
    }, 500);
  });

  grunt.registerTask('default', ['develop', 'buildjs', 'watch']);
  grunt.registerTask('buildjs', ['clean:temp', 'clean:generate', 'requirejs', 'copy:js']);
  grunt.registerTask('package', ['clean', 'buildjs', 'less', 'copy', 'uglify', 'clean:temp']);
  grunt.registerTask('functional-test', ['buildjs', 'less', 'selenium:start', 'express:test:start', 'simplemocha:functional', 'express:test:stop']);
  grunt.registerTask('artifact', ['package', 'compress']);
};
