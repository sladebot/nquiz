'use strict';

var webdriver = require('selenium-webdriver')
  , helper = require('../helper')
  , models = require('../../models')
  , factory = require('../factories')
  , client = require('./client').client()
  , chai = require('chai')
  , expect = chai.expect
  , User = models.User;

describe('Authentication', function() {
  this.timeout(50000);
  var transaction = null;
  var userParams = {email: 'user@example.com', password: 'password', firstName: 'user', lastName: 'one'};
  helper.useTruncation(this, models);

  before(function(done) {
    client.url('http://localhost:5000', done);
  });

  after(function(done) {
    client.end();
    done();
  });

  it('should be able to login', function(done) {

    factory.createUser(transaction, userParams)
    .then(function(user) {
      client.click('a.login')
      .setValue('input[name="email"]', userParams.email)
      .setValue('input[name="password"]', userParams.password)
      .click('button.submit')
      .getTitle(function(err, title) {
        expect(title).to.be.equal('Assessment');
      })
      .click('a.logout', function() {
        done();
      });
    });
  });

  it('should not be able to login with wrong email', function(done) {
    factory.createUser(transaction, userParams)
    .then(function(user) {
      client.click('a.login')
      .setValue('input[name="email"]', 'randomuser@example.com')
      .setValue('input[name="password"]', userParams.password)
      .click('button.submit')
      .getTitle(function(err, title) {
        expect(title).to.be.equal('Login');
        done();
      });
    });
  });

});
