'use strict';

var request = require('supertest')
  , express = require('express')
  , app = express()
  , route = require('../../../route')
  , chai = require('chai')
  , factory = require('../../factories')
  , helper = require('../../helper')
  , models = require('../../../models')
  , async = require('async')
  , expect = chai.expect
  , Category = models.Category
  , _ = require('lodash');

describe('Category controller', function() {
  var getTransaction = helper.bindWebApp(this, models, app);

  describe('create', function() {

    it('unauthorized user not should be able to access', function(done) {
      request(app).post('/categories')
      .send({'name': 'new category'})
      .end(function(err, res) {
        expect(res).to.have.property('status', 403);
        done();
      });
    });

    it('should be able to create category', function(done) {
      var transaction = getTransaction();

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.post('/categories')
        .send({'name': 'new category'})
        .end(function(err, res) {
          expect(res).to.have.property('status', 201);
          expect(res.body).to.have.property('name', 'new category');
          done();
        });
      });
    });

    it('should not be able to create category if name is null', function(done) {
      var transaction = getTransaction();

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.post('/categories')
        .send({'name': null})
        .end(function(err, res) {
          expect(res).to.have.property('status', 400);
          expect(res.body).to.be.deep.equal({name: ['Can\'t be blank']});
          done();
        });
      });
    });
  });

  describe('index', function() {
    it('unauthorized user should not be able to access', function(done) {
      factory.createMultipleCategory(getTransaction())
      .done(function(categories) {
        request(app).get('/categories')
        .end(function(err, res) {
          expect(res).to.have.property('status', 403);
          done();
        });
      });
    });

    it('should be able to get all the categories', function(done) {
      var transaction = getTransaction();

      factory.createMultipleCategory(transaction)
      .done(function(categories) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.get('/categories')
          .end(function(err, res) {
            expect(res).to.have.property('status', 200);
            expect(res.body).to.be.deep.equal(categories);
            done();
          });
        });
      });
    });
  });

  describe('update', function() {

    it('unauthorized user should not be able to access', function(done) {
      var transaction = getTransaction();

      request(app)
      .put('/categories/0')
      .end(function(err, res) {
        expect(res).to.have.property('status', 403);
        done();
      });
    });


    it('should be able to update existing category', function(done) {
      var transaction = getTransaction();

      factory.createCategory(transaction)
      .then(function(category) {

        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.put('/categories/' + category.id)
          .send({'name': 'updated category'})
          .end(function(err, res) {
            expect(res).to.have.property('status', 200);
            category.reload({transaction: transaction})
            .then(function(actualCategory) {
              expect(actualCategory.name).to.be.equal('updated category');
              done();
            });
          });
        });

      });
    });

    it('should not be able to update if name is null', function(done) {
      var transaction = getTransaction();

      factory.createCategory(transaction)
      .then(function(category) {

        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.put('/categories/' + category.id)
          .send({'name': null})
          .end(function(err, res) {
            expect(res).to.have.property('status', 400);
            expect(res.body).to.be.deep.equal({name: ['Can\'t be blank']});
            done();
          });
        });

      });
    });

    it('should not be able to update if category doesn\'t exists', function(done) {
      var transaction = getTransaction();

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.put('/categories/0')
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

  });

  describe('associate', function() {

    it('unauthorized user should not be able to access', function(done) {
      var transaction = getTransaction();

      factory.createQuestionAndCategory(transaction)
      .then(function(questionAndCategory) {
        var category = questionAndCategory.category;
        var question = questionAndCategory.question;

        request(app)
        .post('/categories/' + category.id)
        .send({questionId: question.id})
        .end(function(err, res) {
          expect(res).to.have.property('status', 403);
          done();
        });
      });
    });

    it('should be able to associate question to a category', function(done) {
      var transaction = getTransaction();

      factory.createQuestionAndCategory(transaction)
      .then(function(questionAndCategory) {
        var category = questionAndCategory.category;
        var question = questionAndCategory.question;

        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.post('/categories/' + category.id)
          .send({questionId: question.id})
          .end(function(err, res) {
            expect(res).to.have.property('status', 200);
            done();
          });
        });
      });
    });

    it('should not be able to associate question to a category if category doesn\'t exists', function(done) {
      var transaction = getTransaction();

      factory.createQuestionAndCategory(transaction)
      .then(function(questionAndCategory) {
        var category = questionAndCategory.category;
        var question = questionAndCategory.question;

        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.post('/categories/' + 0)
          .send({questionId: question.id})
          .end(function(err, res) {
            expect(res).to.have.property('status', 404);
            done();
          });
        });
      });
    });

    it('should not be able to associate question to a category if question doesn\'t exists', function(done) {
      var transaction = getTransaction();

      factory.createQuestionAndCategory(transaction)
      .then(function(questionAndCategory) {
        var category = questionAndCategory.category;

        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.post('/categories/' + category.id)
          .send({questionId: 0})
          .end(function(err, res) {
            expect(res).to.have.property('status', 404);
            done();
          });
        });
      });
    });
  });
});
