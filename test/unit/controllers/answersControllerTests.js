"use strict";

var request = require("supertest")
    , express = require("express")
    , app = express()
    , route = require("../../../route")
    , chai = require("chai")
    , factory = require("../../factories")
    , helper = require("../../helper")
    , models = require("../../../models")
    , Question = models.Question
    , Answer = models.Answer
    , async = require('async')
    , expect = chai.expect
    , sprintf = require('sprintf');

describe("Answer controller", function() {
  var getTransaction = helper.bindWebApp(this, models, app);

  describe("create", function() {

    it('unauthorized user should not be able to access', function(done) {
      factory.createQuestion(getTransaction()).then(function(question) {
        request(app)
        .post(sprintf('/questions/%d/answers', question.id))
        .send({questionId: question.id, text: 'answer'})
        .end(function(err, res) {
          expect(res).to.have.property('status', 403);
          done();
        });
      });
    });

    it('should be able to create answer for a question', function(done) {
      var transaction = getTransaction();

      factory.createQuestion(transaction).then(function(question) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.post(sprintf('/questions/%d/answers', question.id))
          .send({questionId: question.id, text: 'answer'})
          .end(function(err, res) {
            expect(res).to.have.property('status', 200);
            expect(res.body).to.have.property('text', 'answer');
            done();
          });
        });
      });
    });

    it('should retrun 404 if the question is not available', function(done) {
      var transaction = getTransaction();

      factory.createQuestion(getTransaction()).then(function(question) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.post("/questions/0/answers")
          .send({text: 'answer'})
          .end(function(err, res) {
            expect(res).to.have.property('status', 404);
            done();
          });
        });
      });
    });

    it('should not be able to create answer without text', function(done) {
      var transaction = getTransaction();

      factory.createQuestion(transaction).then(function(question) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.post(sprintf('/questions/%d/answers', question.id))
          .end(function(err, res) {
            expect(res).to.have.property('status', 400);
            expect(res.body).to.be.deep.equal({text: ["Can't be blank"]});
            done();
          });
        });
      });
    });
  });

  describe("update", function() {

    it('unauthorized user should not be able to access', function(done) {
      var transaction = getTransaction();

      factory.createQuestionWithAnAnswer(getTransaction()).then(function(result) {
        var question = result.question;
        var answer = result.answer;

        request(app)
        .put(sprintf('/questions/%d/answers/%d', question.id, answer.id))
        .send({text: 'updated option'})
        .end(function(err, res) {
          expect(res).to.have.property('status', 403);
          done();
        });
      });
    });

    it('should be able to update answer', function(done) {
      var transaction = getTransaction();

      factory.createQuestionWithAnAnswer(transaction).then(function(result) {

        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          var question = result.question;
          var answer = result.answer;

          server.put(sprintf('/questions/%d/answers/%d', question.id, answer.id))
          .send({text: 'updated option'})
          .end(function(err, res) {
            expect(res).to.have.property('status', 200);
            Answer.find(answer.id, { transaction: getTransaction() }).success(function(updatedAnswer) {
              expect(updatedAnswer.text).to.be.equal('updated option');
              done();
            });
          });
        });
      });
    });

    it('should not be able to update the answer if the answer text is null', function(done) {
      var transaction = getTransaction();

      factory.createQuestionWithAnAnswer(transaction).then(function(result) {
        var question = result.question;
        var answer = result.answer;

        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.put(sprintf('/questions/%d/answers/%d', question.id, answer.id))
          .send({text: null})
          .end(function(err, res) {
            expect(res).to.have.property('status', 400);
            expect(res.body).to.be.deep.equal({text: ["Can't be blank"]});
            done();
          });
        });
      });
    });

    it('should not be able to update the answer if question doesnt exists', function(done) {
      var transaction = getTransaction();

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.put("/questions/0/answers/0")
        .send({text: 'updated option'})
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

    it('should not be able to update the answer if the answer doesnt exists', function(done) {
      var transaction = getTransaction();

      factory.createQuestion(transaction).then(function(question) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.put(sprintf('/questions/%d/answers/%d', question.id, 0))
          .send({text: 'updated option'})
          .end(function(err, res) {
            expect(res).to.have.property('status', 404);
            done();
          });
        });
      });
    });
  });

  describe("mark", function() {
    it('should be able to mark an answer', function(done) {
      factory.createQuestionWithAnAnswer(getTransaction()).then(function(result) {
        var question = result.question;
        var answer = result.answer;

        request(app)
        .post(sprintf('/questions/%d/answers/%d/mark', question.id, answer.id))
        .end(function(err, res) {
          expect(res).to.have.property('status', 200);
          Answer.find({where: { id: answer.id, questionId: question.id }}, { transaction: getTransaction() })
          .success(function(actualAnswer) {
            expect(res.body).to.be.deep.equal(actualAnswer.toJson());
            done();
          });
        });
      });
    });

    it('should not be able to mark an answer if question doesnt exist', function(done) {
      request(app)
      .post(sprintf('/questions/%d/answers/%d/mark', 0, 0))
      .end(function(err, res) {
        expect(res).to.have.property('status', 404);
        done();
      });
    });

    it('should not be able to mark an answer if answer doesnt exist', function(done) {
      factory.createQuestion(getTransaction()).then(function(question) {
        request(app)
        .post(sprintf('/questions/%d/answers/%d/mark', question.id, 0))
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });
  });

  describe('delete', function() {

    it('should be able to delete an existing answer', function(done) {
      var transaction = getTransaction();

      factory.createQuestionWithAnAnswer(transaction).then(function(result) {
        var question = result.question
          , answer = result.answer;

        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.delete(sprintf('/questions/%d/answers/%d', question.id, answer.id))
          .end(function(err, res) {
            expect(res).to.have.property('status', 200);
            Answer.findBy(answer.id, {transaction: transaction})
            .then(function() {
              done(new Error('expected answer to be get deleted but not'));
            })
            .fail(function() {
              done();
            });
          });
        });
      });
    });

    it('unauthorized user should not be able to access', function(done) {
      var transaction = getTransaction();
      factory.createQuestionWithAnAnswer(transaction).then(function(result) {
        var question = result.question
          , answer = result.answer;

        request(app).delete(sprintf('/questions/%d/answers/%d', question.id, answer.id))
        .end(function(err, res) {
          expect(res).to.have.property('status', 403);
          done();
        });
      });
    });


    it('should not be able to delete if the question doesn\'t exists', function(done) {
      var transaction = getTransaction();
      factory.createQuestionWithAnAnswer(transaction).then(function(result) {
        var question = result.question
          , answer = result.answer;

        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.delete(sprintf('/questions/%d/answers/%d', 0, answer.id))
          .end(function(err, res) {
            expect(res).to.have.property('status', 404);
            done();
          });
        });
      });
    });

    it('should not be able to delete if the answer doesn\'t exists', function(done) {
      var transaction = getTransaction();
      factory.createQuestionWithAnAnswer(transaction).then(function(result) {
        var question = result.question
          , answer = result.answer;

        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.delete(sprintf('/questions/%d/answers/%d', question.id, 0))
          .end(function(err, res) {
            expect(res).to.have.property('status', 404);
            done();
          });
        });
      });
    });

  });

});
