'use strict';

var request = require('supertest')
  , express = require('express')
  , app = express()
  , route = require('../../../route')
  , chai = require('chai')
  , factory = require('../../factories')
  , helper = require('../../helper')
  , moment = require('moment')
  , models = require('../../../models')
  , expect = chai.expect
  , Session = models.Session
  , Category = models.Session
  , sprintf = require('sprintf');

describe('Session Controller', function() {
  var getTransaction = helper.bindWebApp(this, models, app);

  describe('index', function() {

    it('unauthorized user should not be able to access', function(done) {
      request(app)
      .get('/sessions')
      .end(function(err, res) {
        expect(res).to.have.property('status', 403);
        done();
      });
    });

    it('should be able to fetch all the active session', function(done) {
      var transaction = getTransaction();
      var params = {
        1: chai.create('session'),
        2: chai.create('session'),
        3: chai.create('session'),
      };

      factory.createMultipleSessions(transaction, params)
      .then(function(sessions) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.get('/sessions')
          .end(function(err, res) {
            expect(res).to.have.property('status', 200);
            expect(res.body).to.have.property('length', 3);
            done();
          });
        });
      });
    });
  });

  describe('create', function() {

    it('unauthorized user should not be able to access', function(done) {
      request(app)
      .post('/sessions')
      .end(function(err, res) {
        expect(res).to.have.property('status', 403);
        done();
      });
    });

    it('should be able to create new session', function(done) {
      var transaction = getTransaction();
      var params = chai.create('session');

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.post('/sessions')
        .send(params)
        .end(function(err, res) {
          expect(res).to.have.property('status', 200);
          expect(res.body.token).to.be.not.equal(null);

          Session.findAll({transaction: transaction})
          .success(function(sessions) {
            expect(sessions).to.have.property('length', 1);
            done();
          });
        });
      });
    });

    it('should not be able to create new session without startDate', function(done) {
      var transaction = getTransaction();
      var params = chai.create('session', { startDate: null });

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.post('/sessions')
        .send(params)
        .end(function(err, res) {
          expect(res).to.have.property('status', 400);
          expect(res.body).to.be.deep.equal({startDate: ['Can\'t be blank']});
          done();
        });
      });
    });

    it('should not be able to create new session without endDate', function(done) {
      var transaction = getTransaction();
      var params = chai.create('session', { endDate: null });

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.post('/sessions')
        .send(params)
        .end(function(err, res) {
          expect(res).to.have.property('status', 400);
          expect(res.body).to.be.deep.equal({endDate: ['Can\'t be blank']});
          done();
        });
      });
    });

    it('should not be able to create new session without duration', function(done) {
      var transaction = getTransaction();
      var params = chai.create('session', { duration: null });

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.post('/sessions')
        .send(params)
        .end(function(err, res) {
          expect(res).to.have.property('status', 400);
          expect(res.body).to.be.deep.equal({duration: ['Can\'t be blank']});
          done();
        });
      });
    });

    it('should not be able to create new session without name', function(done) {
      var transaction = getTransaction();
      var params = chai.create('session', { name: null });

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.post('/sessions')
        .send(params)
        .end(function(err, res) {
          expect(res).to.have.property('status', 400);
          expect(res.body).to.be.deep.equal({name: ['Can\'t be blank']});
          done();
        });
      });
    });

    it('should not be able to create new session without noOfQuestions', function(done) {
      var transaction = getTransaction();
      var params = chai.create('session', { noOfQuestions: null });

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.post('/sessions')
        .send(params)
        .end(function(err, res) {
          expect(res).to.have.property('status', 400);
          expect(res.body).to.be.deep.equal({noOfQuestions: ['Can\'t be blank']});
          done();
        });
      });
    });

  });

  describe('show', function() {

    it('unauthorized should not be able to access', function(done) {
      request(app)
      .get('/sessions/0')
      .end(function(err, res) {
        expect(res).to.have.property('status', 403);
        done();
      });
    });

    it('should be able to retrive an existing session', function(done) {
      var transaction = getTransaction();

      factory.createSession(transaction)
      .then(function(session) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.get(sprintf('/sessions/%s', session.token))
          .end(function(err, res) {
            var actualSession = res.body;
            expect(res).to.have.property('status', 200);
            expect(actualSession).to.have.property('token', session.token);
            done();
          });
        });
      });
    });

    it('should not be able to retrive if the session doesn\'t exist', function(done) {
      var transaction = getTransaction();

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.get('/sessions/abcd1234')
        .end(function(err, res){
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

  });

  describe('update', function() {

    it('unauthorized should not be able to access', function(done) {

      request(app)
      .put('/sessions/0')
      .end(function(err, res) {
        expect(res).to.have.property('status', 403);
        done();
      });

    });

    it('should be able to update existing session', function(done) {
      var transaction = getTransaction();
      var params = chai.create('session');

      factory.createSession(transaction)
      .then(function(session) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.put(sprintf('/sessions/%s', session.token))
          .send(session)
          .end(function(err, res) {

            Session.findBy(session.token, {transaction: transaction})
            .then(function(actualSession) {
              expect(res).to.have.property('status', 200);
              expect(actualSession).to.have.property('duration', params.duration);
              done();
            })
            .fail(function(err) {
              done(new Error(err));
            });
          });
        });
      });
    });

    it('should not be able to update existing session without startDate', function(done) {
      var transaction = getTransaction();
      var params = chai.create('session', {startDate: null});

      factory.createSession(transaction)
      .then(function(session) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.put(sprintf('/sessions/%s', session.token))
          .send(params)
          .end(function(err, res) {
            expect(res).to.have.property('status', 400);
            expect(res.body).to.be.deep.equal({startDate: ['Can\'t be blank']});
            done();
          });
        });
      });
    });

    it('should not be able to update existing session without endDate', function(done) {
      var transaction = getTransaction();
      var params = chai.create('session', {endDate: null});

      factory.createSession(transaction)
      .then(function(session) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.put(sprintf('/sessions/%s', session.token))
          .send(params)
          .end(function(err, res) {
            expect(res).to.have.property('status', 400);
            expect(res.body).to.be.deep.equal({endDate: ['Can\'t be blank']});
            done();
          });
        });
      });
    });

    it('should not be able to update existing session without duration', function(done) {
      var transaction = getTransaction();
      var params = chai.create('session', {duration: null});

      factory.createSession(transaction)
      .then(function(session) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.put(sprintf('/sessions/%s', session.token))
          .send(params)
          .end(function(err, res) {
            expect(res).to.have.property('status', 400);
            expect(res.body).to.be.deep.equal({duration: ['Can\'t be blank']});
            done();
          });
        });
      });
    });

    it('should not be able to update existing session without name', function(done) {
      var transaction = getTransaction();
      var param = chai.create('session', {name: null});

      factory.createSession(transaction)
      .then(function(session) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.put(sprintf('/sessions/%s', session.token))
          .send(param)
          .end(function(err, res) {
            expect(res).to.have.property('status', 400);
            expect(res.body).to.be.deep.equal({name: ['Can\'t be blank']});
            done();
          });
        });
      });
    });

    it('should not be able to update if the session doesnt exists', function(done) {
      var transaction = getTransaction();

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.put('/sessions/abcd134')
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });
  });

  describe('delete', function() {

    it('unauthorized user should not be able to delete', function(done) {
      request(app).delete('/sessions/abcd1234')
      .end(function(err, res) {
        expect(res).to.have.property('status', 403);
        done();
      });
    });

    it('should be able to delete existing session', function(done) {
      var transaction = getTransaction();

      factory.createSession(transaction)
      .done(function(session) {
        helper.loginAsUser(request, app, transaction)
        .then(function(server) {
          server.delete(sprintf('/sessions/%s', session.token))
          .end(function(err, res) {
            expect(res).to.have.property('status', 200);
            Session.findBy(session.token, {transaction: transaction})
            .then(function() {
              done(new Error('expected session to be delete but not'));
            })
            .fail(function() {
              done();
            });
          });
        });
      });
    });

    it('should not be able to delete session which doesn\' exist', function(done) {
      var transaction = getTransaction();

      helper.loginAsUser(request, app, transaction)
      .then(function(server) {
        server.delete('/sessions/0')
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

  });
});
