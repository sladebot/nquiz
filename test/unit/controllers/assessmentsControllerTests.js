'use strict';

var request = require('supertest')
  , express = require('express')
  , app = express()
  , route = require('../../../route')
  , chai = require('chai')
  , factory = require('../../factories')
  , helper = require('../../helper')
  , moment = require('moment')
  , models = require('../../../models')
  , expect = chai.expect
  , Session = models.Session
  , Assessment = models.Assessment
  , sprintf = require('sprintf')
  , _ = require('lodash');

describe('Assessment Controller', function() {
  var getTransaction = helper.bindWebApp(this, models, app);

  describe('new', function(done) {

    it('everyone should be able to access it', function(done) {
      var transaction = getTransaction();

      factory.createSession(transaction)
      .then(function(session) {
        request(app)
        .get(sprintf('/%s', session.token))
        .end(function(err, res) {
          expect(res).to.have.property('status', 200);
          done();
        });
      })
      .fail(function(err) {
        done(new Error(err));
      });
    });

    it('should be 404 if session token is invalid', function(done) {
      request(app)
      .get('/abcd1234')
      .end(function(err, res) {
        expect(res).to.have.property('status', 404);
        done();
      });
    });

  });

  describe('create', function() {

    it('everyone should be able to create new assessment', function(done) {
      var transaction = getTransaction();
      var params = chai.create('assessment');

      factory.createSession(transaction)
      .then(function(session) {
        request(app)
        .post(sprintf('/%s', session.token))
        .send({email: params.email, firstName: params.firstName, lastName: params.lastName})
        .end(function(err, res) {
          expect(res).to.have.property('status', 302);
          done();
        });
      });
    });

    it('should not create new assessment if already exists for same session', function(done) {
      var transaction = getTransaction();
      var params = chai.create('assessment');

      factory.createSession(transaction)
      .then(function(session) {
        factory.createAssessment(transaction, chai.create('assessment', {sessionToken: session.token}))
        .then(function(assessment) {
          request(app)
          .post(sprintf('/%s', session.token))
          .send({email: params.email, firstName: params.firstName, lastName: params.lastName})
          .end(function(err, res) {
            expect(res).to.have.property('status', 302);
            Assessment.all({where: {email: assessment.email, sessionToken: session.token}}, {transaction: transaction})
            .success(function(assessments) {
              expect(assessments).to.have.property('length', 1);
              expect(assessments[0].toJson()).to.be.deep.equal(assessment.toJson());
              done();
            });
          });
        });
      });
    });

    it('should create new assessment if some other session exists by email', function(done) {
      var transaction = getTransaction();
      var params = chai.create('assessment');

      factory.createSession(transaction)
      .then(function(session) {
        factory.createAssessment(transaction, chai.create('assessment'))
        .then(function(assessment) {
          request(app)
          .post(sprintf('/%s', session.token))
          .send({email: params.email, firstName: params.firstName, lastName: params.lastName})
          .end(function(err, res) {
            expect(res).to.have.property('status', 302);
            Assessment.all({where: {email: assessment.email, sessionToken: session.token}}, {transaction: transaction})
            .success(function(assessments) {
              expect(assessments).to.have.property('length', 1);
              expect(assessments[0].token).to.be.not.equal(assessment.token);
              done();
            });
          });
        });
      });
    });

    it('should not be able to create with invalid session token', function(done) {
      request(app)
      .post('/abcd1234')
      .end(function(err, res) {
        expect(res).to.have.property('status', 404);
        done();
      });
    });

    it('should not be able to create without email', function(done) {
      var transaction = getTransaction();
      var params = chai.create('assessment');

      factory.createSession(transaction)
      .then(function(session) {
        request(app)
        .post(sprintf('/%s', session.token))
        .send({email: null, firstName: params.firstName, lastName: params.lastName})
        .end(function(err, res) {
          expect(res).to.have.property('status', 400);
          expect(res.body).to.be.deep.equal({email: [ 'Can\'t be blank' ]});
          done();
        });
      });
    });

    it('should not be able to create without firstName', function(done) {
      var transaction = getTransaction();
      var params = chai.create('assessment');

      factory.createSession(transaction)
      .then(function(session) {
        request(app)
        .post(sprintf('/%s', session.token))
        .send({email: params.email, firstName: null, lastName: params.lastName})
        .end(function(err, res) {
          expect(res).to.have.property('status', 400);
          expect(res.body).to.be.deep.equal({firstName: [ 'Can\'t be blank' ]});
          done();
        });
      });
    });

    it('should not be able to create without lastName', function(done) {
      var transaction = getTransaction();
      var params = chai.create('assessment');

      factory.createSession(transaction)
      .then(function(session) {
        request(app)
        .post(sprintf('/%s', session.token))
        .send({email: params.email, firstName: params.firstName, lastName: null})
        .end(function(err, res) {
          expect(res).to.have.property('status', 400);
          expect(res.body).to.be.deep.equal({lastName: [ 'Can\'t be blank' ]});
          done();
        });
      });
    });

  });

  describe('index', function() {

    it('it should be able to retrive assessment with token', function(done) {
      var transaction = getTransaction();

      factory.createSession(transaction)
      .then(function(session) {
        factory.createAssessment(transaction, chai.create('assessment', {sessionToken: session.token}))
        .then(function(assessment) {
          request(app)
          .get(sprintf('/%s/%s', session.token, assessment.token))
          .end(function(err, res) {
            expect(res).to.have.property('status', 200);
            done();
          });
        });
      });
    });

    it('it should not be able to retrive if session with token doesn\'t exists', function(done) {
      var transaction = getTransaction();

      factory.createAssessment(transaction)
      .then(function(assessment) {
        request(app)
        .get(sprintf('/abcd/%s', assessment.token))
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

    it('it should not be able to retrive if assessment with token doesn\'t exists', function(done) {
      var transaction = getTransaction();

      factory.createSession(transaction)
      .then(function(session) {
        request(app)
        .get(sprintf('/%s/abcd1234', session.token))
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

  });

  describe('update', function() {

    it('should be able to update answers field alone', function(done) {
      var transaction = getTransaction();

      factory.createSessionAndAssessment(transaction, chai.create('session', {noOfQuestions: 3}))
      .then(function(sessionAndAssessment) {
        var session = sessionAndAssessment.session
          , assessment = sessionAndAssessment.assessment
          , answers = {1: {answers: [1, 2]}};

        request(app)
        .put(sprintf('/%s/%s', session.token, assessment.token))
        .send({answers: JSON.stringify(answers)})
        .end(function(err, res) {
          expect(res).to.have.property('status', 200);
          Assessment.findBy({where: {token: assessment.token}}, {transaction: transaction})
          .then(function(updatedAssessment) {
            expect(updatedAssessment.answers).to.be.equal(JSON.stringify(answers));
            done();
          })
          .fail(function(err) {
            done(new Error(err));
          });
        });
      });
    });

    it('should not update if the session token doesn\'t exists', function(done) {
      var transaction = getTransaction();
      factory.createSessionAndAssessment(transaction, chai.create('session', {noOfQuestions: 3}))
      .then(function(sessionAndAssessment) {
        var session = sessionAndAssessment.session
          , assessment = sessionAndAssessment.assessment
          , answers = {1: {answers: [1, 2]}};

        request(app)
        .put(sprintf('/abcd1234/%s', assessment.token))
        .send({answers: JSON.stringify(answers)})
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

    it('should not update if the assessment by token doesn\'t exists', function(done) {
      var transaction = getTransaction();
      factory.createSessionAndAssessment(transaction, chai.create('session', {noOfQuestions: 3}))
      .then(function(sessionAndAssessment) {
        var session = sessionAndAssessment.session
          , answers = {1: {answers: [1, 2]}};

        request(app)
        .put(sprintf('/%s/abcd1234', session.token))
        .send({answers: JSON.stringify(answers)})
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

  });

  describe('questions', function() {

    it('should be able to fetch questions specified in the session', function(done) {
      var transaction = getTransaction();

      factory.createMorethanOneQuestion(transaction, _.range(4))
      .then(function(questions) {
        factory.createSessionAndAssessment(transaction, chai.create('session', {noOfQuestions: 3}))
        .then(function(sessionAndAssessment) {
          var session = sessionAndAssessment.session
            , assessment = sessionAndAssessment.assessment;

          request(app)
          .get(sprintf('/%s/%s/questions', session.token, assessment.token))
          .end(function(err, res) {
            var questions = res.body;
            expect(res).to.have.property('status', 200);
            expect(_.keys(questions)).to.have.property('length', 3);
            done();
          });
        });
      });
    });

    it('should not be able to fetch questions if session token is invalid', function(done) {
      var transaction = getTransaction();

      factory.createSessionAndAssessment(transaction, chai.create('session', {noOfQuestions: 3}))
      .then(function(sessionAndAssessment) {
        var session = sessionAndAssessment.session
          , assessment = sessionAndAssessment.assessment;

        request(app)
        .get(sprintf('/abcd1234/%s/questions', assessment.token))
        .end(function(err, res) {
          var questions = res.body;
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

    it('should not be able to fetch questions if assessment token is invalid', function(done) {
      var transaction = getTransaction();

      factory.createSessionAndAssessment(transaction, chai.create('session', {noOfQuestions: 3}))
      .then(function(sessionAndAssessment) {
        var session = sessionAndAssessment.session
          , assessment = sessionAndAssessment.assessment;

        request(app)
        .get(sprintf('/%s/abcd1234/questions',session.token, assessment.token))
        .end(function(err, res) {
          var questions = res.body;
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

    it('should be able to fetch a specific question', function(done) {
      var transaction = getTransaction();

      factory.createQuestion(transaction)
      .then(function(question) {
        factory.createSessionAndAssessment(transaction)
        .then(function(sessionAndAssessment) {
          var session = sessionAndAssessment.session
            , assessment = sessionAndAssessment.assessment;

          request(app)
          .get(sprintf('/%s/%s/questions/%s', session.token, assessment.token, question.id))
          .end(function(err, res) {
            var expectedQuestion = res.body;
            expect(res).to.have.property('status', 200);
            expect(expectedQuestion).to.be.deep.equal({text: question.text, answers: []});
            done();
          });
        });
      });
    });

  });

  describe('result', function() {

    it('should be able to submit and see the result', function(done) {
      var transaction = getTransaction();

      factory.createSampleQuestionsWithAnswers(transaction)
      .then(function(questionsWithAnswers) {
        factory.createSessionAndAssessment(transaction)
        .then(function(sessionAndAssessment) {
          var session = sessionAndAssessment.session
            , assessment = sessionAndAssessment.assessment
            , answers = factory.generateAnswerFromQuestionsWithAnswers(questionsWithAnswers);

          request(app)
          .post(sprintf('/%s/%s/result', session.token, assessment.token))
          .send({answers: JSON.stringify(answers)})
          .end(function(err, res) {
            expect(res).to.have.property('status', 302);
            expect(res.header).to.be.have.property('location', sprintf('/%s/%s', session.token, assessment.token));

            Assessment.findBy({where: {token: assessment.token}}, {transaction: transaction})
            .then(function(updatedAssessment) {
              var correctAnswers = factory.getCorrectAnswerFromQuestionWithAnswers(questionsWithAnswers);
              expect(updatedAssessment.answers).to.be.equal(JSON.stringify(answers));
              expect(updatedAssessment.result).to.be.equal(JSON.stringify(correctAnswers));
              done();
            }).fail(function(err) {
              done(new Error(err));
            });
          });
        });
      });

    });

    it('should not be able to submit and see the result if session token is invalid', function(done) {
      var transaction = getTransaction();

      factory.createSessionAndAssessment(transaction)
      .then(function(sessionAndAssessment) {
        var session = sessionAndAssessment.session
          , assessment = sessionAndAssessment.assessment
          , answers = {1: {answers: [1, 2]}};

        request(app)
        .post(sprintf('/abcd1234/%s/result', assessment.token))
        .send({answers: JSON.stringify(answers)})
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

    it('should not be able to submit and see the result if assessment token is invalid', function(done) {
      var transaction = getTransaction();

      factory.createSessionAndAssessment(transaction)
      .then(function(sessionAndAssessment) {
        var session = sessionAndAssessment.session
          , assessment = sessionAndAssessment.assessment
          , answers = {1: {answers: [1, 2]}};

        request(app)
        .post(sprintf('/%s/abcd1234/result', session.token))
        .send({answers: JSON.stringify(answers)})
        .end(function(err, res) {
          expect(res).to.have.property('status', 404);
          done();
        });
      });
    });

  });

  describe('summary', function() {

    it('anybody should be able to access', function(done) {
      var transaction = getTransaction();

      factory.createSessionAndAssessment(transaction)
      .then(function(sessionAndAssessment) {
        var session = sessionAndAssessment.session
          , assessment = sessionAndAssessment.assessment;

        request(app)
        .get(sprintf('/%s/%s/summary', session.token, assessment.token))
        .end(function(err, res) {
          expect(res).to.have.property('status', 200);
          done();
        });
      });
    });

    it('should return the assessment summary with the all the questions', function(done) {
      var transaction = getTransaction();

      factory.createSampleQuestionsWithAnswers(transaction)
      .then(function(questionsWithAnswers) {
        factory.createSessionAndAssessment(transaction)
        .then(function(sessionAndAssessment) {
          var session = sessionAndAssessment.session
            , assessment = sessionAndAssessment.assessment
            , userAnswers = factory.generateAnswerFromQuestionsWithAnswers(questionsWithAnswers)
            , result = factory.getCorrectAnswerFromQuestionWithAnswers(questionsWithAnswers);

          assessment.updateAttributes({result: JSON.stringify(result), answers: JSON.stringify(userAnswers)}, {transaction: transaction})
          .then(function() {
            request(app)
            .get(sprintf('/%s/%s/summary', session.token, assessment.token))
            .end(function(err, res) {
              var question = res.body;
              expect(res).to.have.property('status', 200);
              done();
            });
          });
        });
      });
    });

  });

});
