/*exports.hash = function(x, y) {
};*/

'use strict';

var assert = require('assert')
  , chai = require('chai')
  , compare = require('../../../helpers/compare')
  , expect = chai.expect;

describe('compare', function() {

  describe('hash', function() {
    it('should be true by comparing the first level', function(done) {
      var x = {name: 'tom', age: 21}
        , y = {name: 'tom', age: 21};

      expect(compare.hash(x,y)).to.be.equal(true);
      done();
    });

    it('should be false by comparing the first level', function(done) {
      var x = {name: 'jerry', age: 21}
        , y = {name: 'tom', age: 21};

      expect(compare.hash(x,y)).to.be.equal(false);
      done();
    });

    it('should be able to compare array as well', function(done) {
      var x = {name: 'tom', data: [1,2]}
        , y = {name: 'tom', data: [1,2]};

      expect(compare.hash(x,y)).to.be.equal(true);
      done();
    });
  });

  describe('array', function() {

    it('should be true when comparing two similar array', function(done) {
      var array = [1,2,3]
        , anotherArray = [1,2,3];

      expect(compare.array(array, anotherArray)).to.be.equal(true);
      done();
    });

    it('should be true when comparing two similar array in different order', function(done) {
      var array = [1,2,3]
        , anotherArray = [3,2,1];

      expect(compare.array(array, anotherArray)).to.be.equal(true);
      done();
    });

    it('should be fail when array has different elements', function(done) {
      var array = [1,2,4]
        , anotherArray = [3,2,1];

      expect(compare.array(array, anotherArray)).to.be.equal(false);
      done();
    });

    it('should be fail when array length is different elements', function(done) {
      var array = [3,2,1,0]
        , anotherArray = [3,2,1];

      expect(compare.array(array, anotherArray)).to.be.equal(false);
      done();
    });

  });
});
