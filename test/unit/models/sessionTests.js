'use strict';

var assert = require('assert')
  , models = require('../../../models')
  , helper = require('../../helper')
  , factory = require('../../factories')
  , chai = require('chai')
  , expect = chai.expect
  , Session = models.Session
  , moment = require('moment');

describe('Session', function() {

  var getTransaction = helper.useTransactions(this, models);

  describe('Validations', function() {
    it('should be able to create session with start, end date and duration', function(done) {
      var transaction = getTransaction();
      var now = moment();
      var params = chai.create('session');

      factory.createSession(transaction, params)
      .then(function(session) {
        done();
      })
      .fail(function(err) {
        done(new Error('Unable to create session reason: ' + err));
      });
    });

    it('should not be able to create session without start date', function(done) {
      var transaction = getTransaction();
      var now = moment();
      var params = chai.create('session', { startDate: null });

      factory.createSession(transaction, params)
      .then(function(session) {
        done(new Error('able to create session without start date'));
      })
      .fail(function(err) {
        expect(err.startDate).to.be.deep.equal(['Can\'t be blank']);
        done();
      });
    });

    it('should not be able to create session without end date', function(done) {
      var transaction = getTransaction();
      var now = moment();
      var params = chai.create('session', { endDate: null });

      factory.createSession(transaction, params)
      .then(function(session) {
        done(new Error('able to create session without end date'));
      })
      .fail(function(err) {
        expect(err.endDate).to.be.deep.equal(['Can\'t be blank']);
        done();
      });
    });

    it('should not be able to create session without duration', function(done) {
      var transaction = getTransaction();
      var now = moment();
      var params = chai.create('session', { duration: null });

      factory.createSession(transaction, params)
      .then(function(session) {
        done(new Error('able to create session without duration'));
      })
      .fail(function(err) {
        expect(err.duration).to.be.deep.equal(['Can\'t be blank']);
        done();
      });
    });

    it('should not be able to create session without name', function(done) {
      var transaction = getTransaction();
      var params = chai.create('session', { name: null });

      factory.createSession(transaction, params)
      .then(function(session) {
        done(new Error('able to create session without name'));
      })
      .fail(function(err) {
        expect(err.name).to.be.deep.equal(['Can\'t be blank']);
        done();
      });
    });

    it('should not be able to create session without noOfQuestions', function(done) {
      var transaction = getTransaction();
      var params = chai.create('session', { noOfQuestions: null });

      factory.createSession(transaction, params)
      .then(function(session) {
        done(new Error('able to create session without noOfQuestions'));
      })
      .fail(function(err) {
        expect(err.noOfQuestions).to.be.deep.equal(['Can\'t be blank']);
        done();
      });
    });
  });

  describe('token', function() {
    it('should set token when creating session', function(done) {
      var transaction = getTransaction();

      factory.createSession(transaction)
      .then(function(session) {
        expect(session.token).to.be.not.equal(null);
        done();
      })
      .fail(function(err) {
        done(new Error(err));
      });
    });

    it('should not update the token when updating the session', function(done) {
      var transaction = getTransaction();

      factory.createSession(transaction)
      .then(function(session) {
        var actualToken = session.token;
        Session.update({name: 'updated name'}, {token: session.token}, {transaction: transaction})
        .success(function() {
          Session.findBy(session.token, {transaction: transaction})
          .then(function(expectedSession) {
            expect(session.token).to.be.equal(expectedSession.token);
            done();
          });
        });
      });

    });
  });
});
