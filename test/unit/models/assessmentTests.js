'use strict';

var assert = require('assert')
  , models = require('../../../models')
  , helper = require('../../helper')
  , factory = require('../../factories')
  , chai = require('chai')
  , expect = chai.expect
  , Assessment = models.Assessment
  , Session = models.Session
  , moment = require('moment');

describe('Assessment', function() {
  var getTransaction = helper.useTransactions(this, models);

  it('should be able to convert toJson', function(done){
    var transaction = getTransaction();
    var params = chai.create('assessment');

    factory.createAssessment(transaction, params)
    .then(function(assessment) {
      expect(assessment.token).to.be.not.equal(null);
      expect(assessment).to.have.property('email', params.email);
      expect(assessment).to.have.property('firstName', params.firstName);
      expect(assessment).to.have.property('lastName', params.lastName);
      expect(assessment).to.have.property('sessionToken', params.sessionToken);
      expect(assessment).to.have.property('answers', '[]');
      done();
    })
    .fail(function(err) {
      done(new Error(err));
    });
  });

  describe('validations', function() {
    it('should be able to create assessment with email, firstname, lastname, session token and answers', function(done) {
      var transaction = getTransaction();

      factory.createAssessment(transaction)
      .then(function() {
        done();
      })
      .fail(function(err) {
        done(new Error('unable to create assessment reason: ' + JSON.stringify(err)));
      });
    });

    it('should not be able to create assessment without email', function(done) {
      var transaction = getTransaction();
      var params = chai.create('assessment', {email: null});

      factory.createAssessment(transaction, params)
      .then(function() {
        done(new Error('able to create assessment without email'));
      })
      .fail(function(err) {
        done();
      });
    });

    it('should not be able to create assessment without firstname', function(done) {
      var transaction = getTransaction();
      var params = chai.create('assessment', {firstName: null});

      factory.createAssessment(transaction, params)
      .then(function() {
        done(new Error('able to create assessment without firstname'));
      })
      .fail(function(err) {
        done();
      });
    });

    it('should not be able to create assessment without lastname', function(done) {
      var transaction = getTransaction();
      var params = chai.create('assessment', {lastName: null});

      factory.createAssessment(transaction, params)
      .then(function() {
        done(new Error('able to create assessment without lastname'));
      })
      .fail(function(err) {
        done();
      });
    });

    it('should not be able to create assessment without sessionToken', function(done) {
      var transaction = getTransaction();
      var params = chai.create('assessment', {sessionToken: null});

      factory.createAssessment(transaction, params)
      .then(function() {
        done(new Error('able to create assessment without sessionToken'));
      })
      .fail(function(err) {
        done();
      });
    });

    it('should be able to create assessment without answers', function(done) {
      var transaction = getTransaction();
      var params = chai.create('assessment', {answers: null});

      factory.createAssessment(transaction, params)
      .then(function() {
        done();
      })
      .fail(function(err) {
        done(new Error('unable to create assessment without answers reason: ' + err));
      });
    });
  });

  describe('token', function() {
    it('should set token when creating session', function(done) {
      var transaction = getTransaction();

      factory.createAssessment(transaction)
      .then(function(assessment) {
        expect(assessment.token).to.be.not.equal(null);
        done();
      })
      .fail(function(err) {
        done(new Error(err));
      });
    });

    it('should not update the token when updating the session', function(done) {
      var transaction = getTransaction();

      factory.createAssessment(transaction)
      .then(function(assessment) {
        var actualToken = assessment.token;
        Assessment.update({email: 'someoneelse@disney.com'}, {token: assessment.token}, {transaction: transaction})
        .success(function() {
          Assessment.findBy(assessment.token, {transaction: transaction})
          .then(function(expectedAssessment) {
            expect(assessment.token).to.be.equal(expectedAssessment.token);
            done();
          });
        });
      });

    });
  });

  describe('association', function() {

    it('should belong to a session', function(done) {
      var transaction = getTransaction();

      factory.createSessionAndAssessment(transaction)
      .then(function(sessionAndAssessment) {
        var session = sessionAndAssessment.session
          , assessment = sessionAndAssessment.assessment;

        Assessment.findBy({where: assessment.id, include: [Session]}, {transaction: transaction})
        .then(function(assessment) {
          done();
        }).fail(function(err) {
          done(new Error(err));
        });
      }).fail(function(err) {
        done(new Error(err));
      });
    });

  });

  describe('getScore', function() {

    it('should compute correct score based on the answers and result field', function(done) {
      var transaction = getTransaction();
      var answers = {300: {answers: [101]}, 301: {answers: [100]}, 302: {answers: [99]}};
      var result = {300: {answers: [101]}, 301: {answers: [100]}, 302: {answers: [99]}};
      var params = {result: JSON.stringify(result), answers: JSON.stringify(answers) };

      factory.createAssessment(transaction, chai.create('assessment', params))
      .then(function(assessment) {
        expect(assessment.getScore()).to.be.equal(3);
        done();
      }).fail(function(err) {
        done(new Error(err));
      });
    });

    it('should compute correct score based irrespecitve of correct answer being in some other question', function(done) {
      var transaction = getTransaction();
      var answers = {300: {answers: [101]}, 301: {answers: [100]}, 302: {answers: [99]}};
      var result = {300: {answers: [100]}, 301: {answers: [101]}, 302: {answers: [99]}};
      var params = {result: JSON.stringify(result), answers: JSON.stringify(answers) };

      factory.createAssessment(transaction, chai.create('assessment', params))
      .then(function(assessment) {
        expect(assessment.getScore()).to.be.equal(1);
        done();
      }).fail(function(err) {
        done(new Error(err));
      });
    });

    it('should compute correct score inspight of some question having multiple answers', function(done) {
      var transaction = getTransaction();
      var answers = {300: {answers: [101, 98]}, 301: {answers: [100]}, 302: {answers: [99]}};
      var result = {300: {answers: [98, 101]}, 301: {answers: [98]}, 302: {answers: [99]}};
      var params = {result: JSON.stringify(result), answers: JSON.stringify(answers) };

      factory.createAssessment(transaction, chai.create('assessment', params))
      .then(function(assessment) {
        expect(assessment.getScore()).to.be.equal(2);
        done();
      }).fail(function(err) {
        done(new Error(err));
      });
    });

  });
});
