'use strict';

var Q = require('q');

exports.parse = function(value) {
  try {
    return JSON.parse(value);
  } catch(e) {
    return null;
  }
};
