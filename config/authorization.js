'use strict';

var ConnectRoles = require('connect-roles');

module.exports = function() {
  var userRoles = new ConnectRoles({
    failureHandler: function (req, res, action) {
      res.status(403);
      res.send('Access Denied - You don\'t have permission to access this page');
    }
  });

  userRoles.use(function(req, role) {
    return req.user !== undefined;
  });

  return userRoles;
};
