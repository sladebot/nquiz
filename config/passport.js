'use strict';

var LocalStrategy = require('passport-local').Strategy
  , models = require('../models')
  , User = models.User;


module.exports = function(passport) {
  // used to serialize the user for the session
  passport.serializeUser(function(user, done) {
    done(null, user.email);
  });

  // used to deserialize the user
  passport.deserializeUser(function(req, email, done) {
    User.findByEmail(email, {transaction: req.transaction})
    .then(function(user) {
      done(null, user);
    })
    .fail(function(err) {
      done(err, undefined);
    });
  });

  passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  }, function(req, email, password, done) {
    User.findByEmail(email, {transaction: req.transaction})
    .then(function(user) {
      if (user.verifyPassword(password)) {
        req.flash('info', 'Successfully logged in');
        done(null, user);
      } else {
        req.flash('error', 'Invalid email or password');
        done(null, false, {message: 'Invalid password'});
      }
    })
    .fail(function() {
      req.flash('info', 'Invalid email or password');
      done(null, false, {message: 'Invalid user'});
    });
  }));
};
