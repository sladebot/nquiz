'use strict';

define(function(require) {
  var $ = require('jquery')
    , selector = '#session'
    , Modal = require('helper/modal')
    , Vue = require('vue')
    , DateTimePicker = require('helper/datepair')
    , vueHelper = require('helper/vue')
    , Validator = require('helper/validator')
    , newSession = require('app/session/newSession')
    , existingSession = require('app/session/existingSession')
    , momentHelper = require('helper/moment')
    , _ = require('lodash');

  var createSessionViewModel = function(methods) {
    return new Vue({
      el: selector,
      data: null,
      methods: methods
    });
  };

  var Session = function() {
    var methods = {
      edit: $.proxy(this.edit, this),
      save: $.proxy(this.save, this)
    };
    var dateTimePicker = new DateTimePicker(selector, {
      onUpdate: $.proxy(this.updateDateAndTime, this)
    });

    this.validator = new Validator('#sessionForm');
    this.viewModel = createSessionViewModel(methods);
  };

  Session.prototype =  {
    edit: function(session) {
      return this.load(existingSession, session.token);
    },
    new: function() {
      return this.load(newSession, null);
    },
    load: function(sessionType, sessionId) {
      var defer = $.Deferred()
        , that = this;

      that.modal = new Modal(selector);
      that.sessionId = sessionId;
      that.sessionType = sessionType;
      sessionType.load(that.sessionId).then(function(session) {
        var instance = {
          mode: that.sessionType.type,
          startDate: momentHelper.getDate(session.startDate),
          startTime: momentHelper.getTime(session.startDate),
          endDate: momentHelper.getDate(session.endDate),
          endTime: momentHelper.getTime( session.endDate)
        };
        that.viewModel.$data = $.extend(session, instance);
        that.modal.show().done(function(item) {
          defer.resolve(item);
        });
      });
      return defer;
    },
    save: function(item, e) {
      var that = this;
      var params = $.extend(_.clone(item.$data), momentHelper.mergeDateTime(item.$data));
      e.preventDefault();
      that.sessionType.save(this.sessionId, params).then(function(session) {
        that.modal.hide(session || params);
      });
    },
    updateDateAndTime: function(data) {
      $.extend(this.viewModel.$data, data);
    },
  };

  return Session;
});
