'use strict';

define(function(require) {

  var $ = require('jquery')
    , textUtil = require('sprintf');

  return {
    type: 'Edit',
    newAnswer: function(questionId) {
      var defer = $.Deferred();
      $.ajax({
        type: 'POST',
        url: textUtil.sprintf('/questions/%d/answers', questionId),
        data: {text: ''}
      }).success(function(answer) {
        defer.resolve(answer);
      });
      return defer;
    },
    toggleAnswer: function(questionId, answer) {
      var defer = $.Deferred();
      var that = this;

      $.post(textUtil.sprintf('/questions/%d/answers/%d/mark', questionId, answer.id))
      .success(function(updatedAnswer) {
        defer.resolve(updatedAnswer);
      });
      return defer;
    },
    updateAnswer: function(questionId, answer) {
      var defer = $.Deferred();

      $.ajax({
        type: 'PUT',
        url: textUtil.sprintf('/questions/%d/answers/%d', questionId, answer.id),
        data: {text: answer.text}
      }).success(function() {
        defer.resolve();
      });
      return defer;
    },
    deleteAnswer: function(questionId, answer) {
      var defer = $.Deferred();
      var that = this;

      $.ajax({
        type: 'DELETE',
        url: textUtil.sprintf('/questions/%d/answers/%d', questionId, answer.id)
      }).done(function() {
        defer.resolve(answer.$data);
      });
      return defer;
    },
    load: function(questionId) {
      var defer = $.Deferred();

      $.get(textUtil.sprintf('/questions/%d', questionId)).done(function(item) {
        defer.resolve(item);
      });
      return defer;
    },
    save: function(questionId, question) {
      var defer = $.Deferred();

      $.ajax({
        type: 'PUT',
        url: textUtil.sprintf('/questions/%d', questionId),
        data: {text: question.$data.question.text, reason: question.$data.question.reason}
      }).success(function() {
        defer.resolve();
      });
      return defer;
    }
  };

});
