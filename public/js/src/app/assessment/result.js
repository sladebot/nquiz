'use strict';

define(function(require) {

  var Vue = require('vue')
    , vueHelper = require('helper/vue')
    , selector = '#results'
    , $ = require('jquery');

  var createResultViewModel = function(data, methods) {
    return new Vue({
      el: selector,
      data: data,
      methods: methods,
      computed:  {
        type: {
          $get: function() {
            return 1;
          }
        }
      }
    });
  };

  var Result = function(result) {
    var methods = {};
    this.viewModel = createResultViewModel($.extend({loaded: true}, result) ,methods);
  };

  return Result;
});
