'use strict';

require(['jquery', 'app/base', 'app/question/manageQuestions'],
  function($, base, ManageQuestions) {
    var questions = new ManageQuestions({});

    $(function() { questions.load(); });
  }
);
