requirejs.config({
  baseUrl: 'js/src/lib',
  enforceDefine : true,
  paths: {
    app: '../app',
    helper: '../helper',
    bootstrap: '../../../components/bootstrap/js',
    jquery: '../../../components/jquery/dist/jquery',
    underscore: '../../../components/underscore/underscore',
    vue: '../../../components/vue/dist/vue',
    sprintf: '../../../components/sprintf/dist/sprintf.min',
    timepicker: '../../../components/jt.timepicker/jquery.timepicker',
    datepicker: '../../../components/bootstrap-datepicker/js/bootstrap-datepicker',
    datepair: '../../../components/datepair/jquery.datepair',
    parsley: '../../../components/parsleyjs/dist/parsley',
    moment: '../../../components/momentjs/moment',
    lodash: '../../../components/lodash/dist/lodash'
  },
  shim: {
    jquery: {
      exports: '$'
    },
    knockout: {
      exports: 'ko'
    },
    underscore: {
      exports: '_'
    },
    vue: {
      exports: 'vue'
    },
    sprintf: {
      exports: 'sprintf'
    }
  },
});
