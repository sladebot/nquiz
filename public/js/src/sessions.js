'use strict';

require(['jquery', 'app/base', 'app/session/manageSessions'],
  function($, base, ManageSessions) {
    var sessions = new ManageSessions({});

    $(function() { sessions.load(); });
  }
);
