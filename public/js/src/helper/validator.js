'use strict';

define(function(require) {
  var $ = require('jquery')
    , parsley = require('parsley');

  return function(form) {
    $(function() {
      $(form).parsley();
    });
  };
});
