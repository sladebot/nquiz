/* jshint strict: false, laxcomma: true */

define(function(require) {

  var $ = require('jquery')
    , bootstrapModal = require('bootstrap/modal');

  var Modal = function(selector, options) {
    this.options = {
      onClose: $.noop
    };
    this.element = $(selector);
    this.defer = $.Deferred();
    $.extend(this.options, options);
    $(this.element).on('click', $.proxy(this.handleClick, this));
    $(this.element).on('hidden.bs.modal', $.proxy(this.onHide, this));
  };

  Modal.prototype = {
    handleClick: function(evt) {
      var element = $(evt.target);
      var action = element.data('action');
      if (action) {
        this.hide();
      }
    },
    show: function() {
      $(this.element).modal({show: true, keyboard: true, backdrop: 'static'});
      return this.defer;
    },
    hide: function(data) {
      this.reseolveData = data;
      $(this.element).modal('hide');
    },
    onHide: function() {
      if(this.reseolveData) {
        this.defer.resolve(this.reseolveData);
      }
      this.options.onClose();
    }
  };

  return Modal;
});
