/* jshint strict: false */

define(function(require) {
  var Vue = require('vue')
    , $ = require('jquery')
    , textUtil = require('sprintf')
    , momentHelper = require('helper/moment');

  var focusElement = function(el) {
    setTimeout(function () { $(el).focus(); }, 300);
  };

  Vue.directive('focus', {
    bind: function(value) {
      if(value) { return; }
      focusElement(this.el);
    },
    update: function(value) {
      if(value) { return; }
      focusElement(this.el);
    }
  });

  Vue.filter('date', function(value) {
    return momentHelper.getDate(value);
  });

  Vue.filter('time', function(value) {
    return momentHelper.getTime(value);
  });

  Vue.filter('dateTime', function(value) {
    return momentHelper.getDateTime(value);
  });

  Vue.filter('append', function(value, textToAppend) {
    return textUtil.sprintf('%s%s',textToAppend,value);
  });

  Vue.filter('userFriendlyIndex', function(value) {
    return value + 1;
  });
  
});
